const namespace = "@@venteprivee/marvel";

export const LOAD = `${namespace}/load`;
export const SUCCESS = `${namespace}/success`;
export const FAIL = `${namespace}/fail`;